<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>String PHP</title>
</head>
<body>
    <h1>Berlatih String PHP</h1>
    <?php   
        echo "<h3> Soal No 1</h3>";

        $first_sentence = "Hello PHP! <br>" ; // Panjang string 10, jumlah kata: 2
        $panjangkalimat = strlen($first_sentence);
        $jumlahkata = str_word_count($first_sentence);
        echo "kalimat : $first_sentence";
        echo "panjang kalimat : $panjangkalimat <br>";
        echo "jumlah kata : $jumlahkata<br><br>";
        $second_sentence = "I'm ready for the challenges <br>"; // Panjang string: 28,  jumlah kata: 5
        $panjangkalimat1 = strlen($second_sentence);
        $jumlahkata1 = str_word_count($second_sentence);
        echo "kalimat : $second_sentence";
        echo "jumlah kalimat : $panjangkalimat1 <br>";
        echo "jumlah kata : $jumlahkata1";

        
        echo "<h3> Soal No 2</h3>";
        /* 
            SOAL NO 2
            Mengambil kata pada string dan karakter-karakter yang ada di dalamnya. 
            
            
        */
        $string2 = "I love PHP";
        
        echo "<label>String: </label> \"$string2\" <br>";
        echo "Kata pertama: " . substr($string2, 0, 1) . "<br>" ; 
        // Lanjutkan di bawah ini
        echo "Kata kedua: " . substr($string2, 2, 4) ; 
        echo "<br> Kata Ketiga: " . substr($string2, 6, 4) ;

        echo "<h3> Soal No 3 </h3>";
        /*
            SOAL NO 3
            Mengubah karakter atau kata yang ada di dalam sebuah string.
        */
        $string3 = "PHP is old but sexy!";
        echo "String: \"$string3\" <br>"; 
        echo "kalimat string yang diganti : " . str_replace("sexy" , "awesome" , $string3);
        // OUTPUT : "PHP is old but awesome"

    ?>
</body>
</html>